var app = angular.module('teqnihome.services', []);

app.service('AuthService', function($q, $http, $timeout) {
    let LOCAL_TOKEN_KEY = 'token';
    let LOCAL_USER = 'tquser';

    let API = function(method, url, params, data) {
      var deferred = $q.defer();
      $http({
        method: method,
        url: CONSTANTS.API_URL + url,
        params: params,
        data: data,
        timeout: 10000,
      }).then(function(data) {
        deferred.resolve(data);
      }, function(error) {
        deferred.reject(error);
      });
      return deferred.promise;
    };
    let isAuthenticated = function(){
      return getUser() && window.localStorage.getItem(LOCAL_TOKEN_KEY);
    }

    let getUser = function(){
      let user = angular.fromJson(window.localStorage.getItem(LOCAL_USER));
      if(user) return user;
      return null;
    };

    let loginUser = function(data) {
      let deferred = $q.defer();

      // mocking requests
      let data_ = {
        access_token: 'ow57e6j2hvi8pjaskvbl',
        mail: data.mail
      }
      window.localStorage.setItem(LOCAL_TOKEN_KEY, data_.access_token);
      window.localStorage.setItem(LOCAL_USER,angular.toJson(data_));
      isAuthenticated = true;
      $timeout(function() {
        deferred.resolve(data_);
      }, 200);
       return deferred.promise;
    };

    //mocking registration api
    let registerUser = function(data, mock = false) {
        let deferred = $q.defer();
         $timeout(function() {
          deferred.resolve(data);
        }, 200);
        return deferred.promise;
    };

    //logout
    let logout = function() {
      isAuthenticated = false;
      window.localStorage.removeItem(LOCAL_TOKEN_KEY);
    };

    //forgot password
    let forgotPassword = function(data) {
      let deferred = $q.defer();
      $timeout(function() {
        deferred.resolve(data);
      }, 200);
      return deferred.promise;
    };

    return {
      loginUser: loginUser,
      registerUser: registerUser,
      logout: logout,
      forgotPassword: forgotPassword,
      isAuthenticated: isAuthenticated,
      getUser: getUser,
    };
  })
  .factory('AuthInterceptor', function($rootScope, $q, AUTH_EVENTS, $timeout) {
    return {
      responseError: function(response) {
        $rootScope.$broadcast({
          401: AUTH_EVENTS.notAuthenticated,
          403: AUTH_EVENTS.notAuthorized
        }[response.status], response);
        return $q.reject(response);
      },
      response: function(response) {
        /* using timeout to delay the response.
         * It is only for emulating and showing the spinner
         * because http response is very fast for localhost
         */
        // return $timeout(function() {
        return response;
        // }, 10000);

      }
    };
  })






  .config(function($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
  });
