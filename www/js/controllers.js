var app = angular.module('teqnihome.controllers', []);

app.controller('AppCtrl', function($scope, $rootScope, AuthService) {
  ionic.material.ink.displayEffect();
  $rootScope.isAuthenticated = AuthService.isAuthenticated();

})



//registration controller
app.controller('registerCtrl', function($scope, vcRecaptchaService, $rootScope, $window, $ionicLoading, AuthService, $state, $ionicPopup) {
  $scope.user = {};
  $scope.register = function() {
    $ionicLoading.show();
    $scope.user.name = $scope.user.fname + ' ' + $scope.user.lname;
    AuthService.registerUser($scope.user, true).then(function(data) {
      $ionicPopup.alert({
        title: 'Registration Successful!',
        template: 'Please login!'
      });
      $state.go('app.login');
    }, function(data) {
      $ionicPopup.alert({
        title: 'Registration failed!',
        template: 'Make sure inputs are valid and try again!'
      });
    })
    .finally(function() {
      $ionicLoading.hide();
    });
  };

  //reloads recaptcha if it expires
  $scope.cbExpiration = function() {
    console.info('Captcha expired. Resetting response object');
    vcRecaptchaService.reload($scope.widgetId);
  };

});

// login controller
app.controller('loginCtrl', function($scope, $rootScope, $ionicLoading, $state, AuthService, $ionicPopup) {
  $scope.user = {};
  $scope.login = function() {
    $ionicLoading.show();
    AuthService.loginUser($scope.user, true).then(function(data) {
      $state.go('app.home');
    }, function(data) {
      $ionicPopup.alert({
          title: 'Login failed!',
          template: 'Please check your credentials!'
        });
      })
      .finally(function() {
          $ionicLoading.hide();
        });
  };
});

//forgot password controller
app.controller('forgotCtrl', function($scope, $rootScope, $ionicPopup, $ionicLoading, $cordovaToast, AuthService) {
  $scope.user = {};
  $scope.forgotPassword = function() {
    $ionicLoading.show();
    AuthService.forgotPassword($scope.user).then(function(data) {
      $ionicPopup.alert({
        title: 'Link sent successfully!',
        template: 'Please check your mail'
      });
    }, function(data) {
      $ionicPopup.alert({
        title: 'Request failed',
        template: 'Please try again!'
      });
    })
    .finally(function() {
      $ionicLoading.hide();
    });
  };
});

app.controller('homeCtrl', function($scope, $rootScope, $ionicLoading, $cordovaToast, AuthService) {
  $scope.username = AuthService.getUser().mail;
});

app.controller('logoutCtrl', function($scope, $rootScope, $state, $ionicHistory, AuthService) {
  AuthService.logout();
  $ionicHistory.clearCache();
  $ionicHistory.clearHistory();
  $ionicHistory.nextViewOptions({
    disableBack: true,
    historyRoot: true
  });
  $state.go('app.login');
})
