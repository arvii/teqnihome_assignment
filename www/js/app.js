window.global = {
  LOGIN: 'http://localhost/api/login',
  REGISTER: 'http://localhost/api/register',
  FORGOT : 'http://localhost/api/forgot',
}

angular.module('teqnihome', ['ionic', 'ngCordova','vcRecaptcha', 'teqnihome.controllers', 'teqnihome.services'])

  .run(function($ionicPlatform, $rootScope, $ionicLoading, AuthService, $state, $ionicModal, $ionicPopup) {
    $ionicPlatform.ready(function() {

      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
        document.addEventListener("deviceready", function() {
          $rootScope.online = false;
          $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
            $rootScope.online = true;
          });

          // listen for Offline event
          $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
            $rootScope.online = false;
          });
        }, false);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        // console.log(AuthService.isAuthenticated());
        $rootScope.isAuthenticated = AuthService.isAuthenticated();
        if (toState.views.data && angular.isObject(toState.views.data)) {
          if (toState.views.data.requireLogin && !AuthService.isAuthenticated()) {
            event.preventDefault();
            $state.go('app.login');
          }
          if (toState.views.data.afterLogin == false) {
            event.preventDefault();
          }
        }
      });
    });
  })

  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('app', {
        url: '/app',
        abstract: false,
        cache: false,
        templateUrl: 'templates/main/menu.html',
        controller: 'AppCtrl'
      })
      .state('app.login', {
        url: '/login',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/main/login.html',
            controller: 'loginCtrl'
          }
        },
        data: {
          requireLogin: false,
          afterLogin: false,
        }
      })
      .state('app.register', {
        url: '/register',
        views: {
          'menuContent': {
            templateUrl: 'templates/main/register.html',
            controller: 'registerCtrl'
          }
        },
        data: {
          requireLogin: false,
          afterLogin : false,
        }
      })
      .state('app.forgot', {
        url: '/forgot',
        views: {
          'menuContent': {
            templateUrl: 'templates/main/forgot.html',
            controller: 'forgotCtrl'
          },
        },
        data: {
          requireLogin: false,
          afterLogin : false,
        }
      })
      .state('app.logout', {
        url: '/logout',
        views: {
          'menuContent': {
            controller: 'logoutCtrl'
          }
        },
        data: {
          requireLogin: true,
          afterLogin: true,
        }
      })
      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'templates/main/home.html',
            controller: 'homeCtrl'
          }
        },
        data: {
          requireLogin: true,
          afterLogin: true,
        }
      })

    $urlRouterProvider.otherwise('/app/login');
  })
